﻿namespace ChangeFilesFolder
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.btnIncluirPrefixo = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnSubstituir = new DevExpress.XtraEditors.SimpleButton();
            this.txtSubstituirPor = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txtSubstituirDe = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.chkMp3 = new DevExpress.XtraEditors.CheckEdit();
            this.btnListarArquivos = new DevExpress.XtraEditors.SimpleButton();
            this.txtPrefixo = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtDiretorio = new DevExpress.XtraEditors.ButtonEdit();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubstituirPor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubstituirDe.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkMp3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrefixo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiretorio.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControl1
            // 
            this.gridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl1.Location = new System.Drawing.Point(0, 107);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(677, 236);
            this.gridControl1.TabIndex = 2;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // btnIncluirPrefixo
            // 
            this.btnIncluirPrefixo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnIncluirPrefixo.Location = new System.Drawing.Point(521, 39);
            this.btnIncluirPrefixo.Name = "btnIncluirPrefixo";
            this.btnIncluirPrefixo.Size = new System.Drawing.Size(84, 23);
            this.btnIncluirPrefixo.TabIndex = 3;
            this.btnIncluirPrefixo.Text = "Incluir Prefixo";
            this.btnIncluirPrefixo.Click += new System.EventHandler(this.BtnRenomearArquivosClick);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.btnSubstituir);
            this.panelControl1.Controls.Add(this.txtSubstituirPor);
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Controls.Add(this.txtSubstituirDe);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.btnIncluirPrefixo);
            this.panelControl1.Controls.Add(this.chkMp3);
            this.panelControl1.Controls.Add(this.btnListarArquivos);
            this.panelControl1.Controls.Add(this.txtPrefixo);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.txtDiretorio);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(677, 101);
            this.panelControl1.TabIndex = 6;
            // 
            // btnSubstituir
            // 
            this.btnSubstituir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSubstituir.Location = new System.Drawing.Point(521, 67);
            this.btnSubstituir.Name = "btnSubstituir";
            this.btnSubstituir.Size = new System.Drawing.Size(84, 23);
            this.btnSubstituir.TabIndex = 15;
            this.btnSubstituir.Text = "Substituir";
            this.btnSubstituir.Click += new System.EventHandler(this.BtnSubstituirClick);
            // 
            // txtSubstituirPor
            // 
            this.txtSubstituirPor.Location = new System.Drawing.Point(273, 70);
            this.txtSubstituirPor.Name = "txtSubstituirPor";
            this.txtSubstituirPor.Size = new System.Drawing.Size(140, 20);
            this.txtSubstituirPor.TabIndex = 14;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(251, 73);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(16, 13);
            this.labelControl4.TabIndex = 13;
            this.labelControl4.Text = "por";
            // 
            // txtSubstituirDe
            // 
            this.txtSubstituirDe.Location = new System.Drawing.Point(105, 70);
            this.txtSubstituirDe.Name = "txtSubstituirDe";
            this.txtSubstituirDe.Size = new System.Drawing.Size(140, 20);
            this.txtSubstituirDe.TabIndex = 12;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(9, 73);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(49, 13);
            this.labelControl3.TabIndex = 11;
            this.labelControl3.Text = "Substituir:";
            // 
            // chkMp3
            // 
            this.chkMp3.EditValue = true;
            this.chkMp3.Location = new System.Drawing.Point(440, 12);
            this.chkMp3.Name = "chkMp3";
            this.chkMp3.Properties.Caption = "Só MP3";
            this.chkMp3.Size = new System.Drawing.Size(75, 19);
            this.chkMp3.TabIndex = 10;
            // 
            // btnListarArquivos
            // 
            this.btnListarArquivos.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnListarArquivos.Location = new System.Drawing.Point(521, 9);
            this.btnListarArquivos.Name = "btnListarArquivos";
            this.btnListarArquivos.Size = new System.Drawing.Size(84, 23);
            this.btnListarArquivos.TabIndex = 8;
            this.btnListarArquivos.Text = "Listar";
            this.btnListarArquivos.Click += new System.EventHandler(this.BtnListarArquivosClick);
            // 
            // txtPrefixo
            // 
            this.txtPrefixo.Location = new System.Drawing.Point(105, 42);
            this.txtPrefixo.Name = "txtPrefixo";
            this.txtPrefixo.Size = new System.Drawing.Size(140, 20);
            this.txtPrefixo.TabIndex = 9;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(9, 45);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(38, 13);
            this.labelControl2.TabIndex = 8;
            this.labelControl2.Text = "Prefixo:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(9, 19);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(45, 13);
            this.labelControl1.TabIndex = 7;
            this.labelControl1.Text = "Diretório:";
            // 
            // txtDiretorio
            // 
            this.txtDiretorio.Location = new System.Drawing.Point(105, 12);
            this.txtDiretorio.Name = "txtDiretorio";
            this.txtDiretorio.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtDiretorio.Size = new System.Drawing.Size(329, 20);
            this.txtDiretorio.TabIndex = 6;
            this.txtDiretorio.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.TxtDiretorioButtonClick);
            // 
            // panelControl2
            // 
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl2.Location = new System.Drawing.Point(0, 349);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(677, 33);
            this.panelControl2.TabIndex = 7;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(677, 382);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.gridControl1);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Alterar Nomes de Arquivos";
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubstituirPor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubstituirDe.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkMp3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrefixo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiretorio.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.SimpleButton btnIncluirPrefixo;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.TextEdit txtPrefixo;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.ButtonEdit txtDiretorio;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.SimpleButton btnListarArquivos;
        private DevExpress.XtraEditors.CheckEdit chkMp3;
        private DevExpress.XtraEditors.SimpleButton btnSubstituir;
        private DevExpress.XtraEditors.TextEdit txtSubstituirPor;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit txtSubstituirDe;
        private DevExpress.XtraEditors.LabelControl labelControl3;

    }
}

