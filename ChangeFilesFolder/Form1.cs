﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ChangeFilesFolder
{
    public partial class Form1 : Form
    {
        private string _path;

        public Form1()
        {
            InitializeComponent();
        }

        private void TxtDiretorioButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            try
            {
                _path = GetPath();
                txtDiretorio.EditValue = _path;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ValidaDiretorio()
        {
            if (!string.IsNullOrEmpty(txtDiretorio.Text))
                _path = txtDiretorio.Text;

            if (!string.IsNullOrEmpty(_path))
                return;

            throw new Exception("Selecione um Diretorio.");
        }

        private string GetPath()
        {
            var folder = new FolderBrowserDialog();

            if (folder.ShowDialog() == DialogResult.OK)
                return folder.SelectedPath;

            return string.Empty;
        }

        private void MostraArquivos()
        {
            ValidaDiretorio();

            var diretorio = new DirectoryInfo(_path);

            FileInfo[] files;

            files = chkMp3.Checked ? diretorio.GetFiles("*.mp3") : diretorio.GetFiles();

            var arquivos = files
                .OrderBy(c => c.Name)
                .Select(c => new { c.Directory, c.Name })
                .ToList();

            gridControl1.DataSource = null;
            gridControl1.DataSource = arquivos;

            gridView1.BestFitColumns();
        }

        private void BtnRenomearArquivosClick(object sender, EventArgs e)
        {
            try
            {
                if (!IncluirPrefixo())
                {
                    MessageBox.Show("Digite um prefixo.");
                    return;
                }

                MostraArquivos();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private bool IncluirPrefixo()
        {
            var prefixo = txtPrefixo.Text;
            if (string.IsNullOrEmpty(prefixo))
                return false;

            var arquivos = new DirectoryInfo(_path).GetFiles();

            foreach (var fileInfo in arquivos.Where(c => !c.Name.StartsWith(prefixo)))
            {
                var oldName = fileInfo.Name;

                var newName = prefixo + oldName;
                var newFullFileName = Path.Combine(_path, newName);

                File.Move(fileInfo.FullName, newFullFileName);
            }

            return true;
        }

        private void BtnListarArquivosClick(object sender, EventArgs e)
        {
            try
            {
                ValidaDiretorio();
                MostraArquivos();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void BtnSubstituirClick(object sender, EventArgs e)
        {
            try
            {
                ValidaSubstituir();

                if (!SubstituiNomesArquivos())
                    return;

                MostraArquivos();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private bool SubstituiNomesArquivos()
        {
            var de = txtSubstituirDe.Text;
            var por = txtSubstituirPor.Text;

            var arquivos = new DirectoryInfo(_path).GetFiles();

            foreach (var fileInfo in arquivos)
            {
                var oldName = fileInfo.Name;

                var newName = oldName.Replace(de, por);
                var newFullFileName = Path.Combine(_path, newName);

                File.Move(fileInfo.FullName, newFullFileName);
            }

            return true;
        }

        private void ValidaSubstituir()
        {
            if (string.IsNullOrEmpty(txtSubstituirDe.Text))
                throw new Exception("Preencher pelo menos campo Substituir De.");
        }
    }
}
